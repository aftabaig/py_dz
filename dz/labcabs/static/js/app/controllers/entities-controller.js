lc.controller("EntitiesController", ['$scope', '$rootScope', '$localStorage', 'EntityService', 'entities', function($scope, $rootScope, $localStorage, EntityService, entities) {

    $scope.entities = entities;

    $scope.delete = function(index) {
        var entity = $scope.entities[index];
        if (entity)
        {
            EntityService.delete(entity.id)
            .then(function() {
                $scope.entities.splice(index, 1);
            })
        }
    }

}]);