lc.controller("ConsignmentsController", ['$scope', '$rootScope', '$localStorage', 'ConsignmentService', 'consignments', function($scope, $rootScope, $localStorage, ConsignmentService, consignments) {

    $scope.consignments = consignments;
    
    $scope.delete = function(index) {
        var consignment = $scope.consignments[index];
        if (consignment)
        {
            ConsignmentService.delete(consignment.id)
            .then(function() {
                $scope.consignments.splice(index, 1);
            })
        }
    }

}]);