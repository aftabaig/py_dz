dz.controller("ReportController", ['$scope', '$rootScope', '$localStorage', 'LeaseService', 'ReportService', 'ReportManager', 'leases', function($scope, $rootScope, $localStorage, LeaseService, ReportService, ReportManager, leases) {

    $rootScope.activeMenu = "Reports";
    $rootScope.currentUser = $localStorage.user;

    $scope.leases = leases;

    $scope.years = []
    $scope.getYears = function() {
        currentYear = new Date().getFullYear();
        for (y=currentYear-5; y<=currentYear+5; y++) {
            $scope.years.push(y);
        }
    }
    $scope.getYears();

    $scope.selectLease = function(lease) {
        $scope.selectedLease = lease;
    }

    $scope.selectYear = function(year) {
        $scope.selectedYear = year;
    }

    $scope.progressBar = {
        width: "0%"
    }

    $scope.disableControls = false;

    $scope.showReport = function() {

        var wells, manifoldPressures;
        var januaryData, februaryData, marchData, aprilData, mayData, juneData, julyData, augustData, septemberData, octoberData, novemberData, decemberData;
        var wellsData, pressureData;
        var notes;
        var i=0;
        var progress = 0;
        var processes = 17;

        $scope.disableControls = true;

        ReportService.monthlyReport($scope.selectedLease.id, 1, $scope.selectedYear)
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            januaryData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 2, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            februaryData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 3, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            marchData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 4, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            aprilData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 5, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            mayData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 6, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            juneData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 7, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            julyData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 8, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            augustData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 9, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            septemberData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 10, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            octoberData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 11, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            novemberData = data;
            return ReportService.monthlyReport($scope.selectedLease.id, 12, $scope.selectedYear)

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            decemberData = data;
            return ReportService.injectionReport($scope.selectedLease.id,$scope.selectedYear);

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            wells = data.names;
            wellsData = data.data;
            return ReportService.manifoldPressureReport($scope.selectedLease.id, $scope.selectedYear);

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            manifoldPressures = data.names;
            pressureData = data.data;
            return ReportService.notesData($scope.selectedLease.id, $scope.selectedYear);

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            notes = data;
            return ReportService.ruralWaterReport($scope.selectedLease.id, $scope.selectedYear);

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            ruralWater = data.data;
            return ReportService.oilProductionReport($scope.selectedLease.id, $scope.selectedYear);

        })
        .then(function(data) {

            i++;
            progress = parseInt((i/processes)*100);
            $scope.progressBar = {
                width: progress + "%"
            }

            oilProduced = data.data;

            var wellInjectionData = ReportManager.wellInjectionSummary(wells, wellsData, "Well Injection Summary");
            var wellInjectionPDF = wellInjectionData.pdf;
            var avgInjData = wellInjectionData.inj_avg;

            var reportData = {
                pageSize: 'A1',
                footer: function(current, total) {
                    return {
                        text: current.toString() + " of " + total.toString(),
                        alignment: 'center'
                    }
                },
                header: {
                    text: "D&Z Exploration",
                    alignment: 'center',
                    margin: [0, 10, 0, 0]
                },
                content: [
                    ReportManager.titlePage($scope.selectedLease.name, "Annual Report " + $scope.selectedYear),
                    ReportManager.titlePage("Monthly Injection Report", "January-" + $scope.selectedYear + " to " + "December-" + $scope.selectedYear),
                    ReportManager.monthlyInjectionReport(januaryData, 'January'),
                    ReportManager.monthlyInjectionReport(februaryData, 'February'),
                    ReportManager.monthlyInjectionReport(marchData, 'March'),
                    ReportManager.monthlyInjectionReport(aprilData, 'April'),
                    ReportManager.monthlyInjectionReport(mayData, 'May'),
                    ReportManager.monthlyInjectionReport(juneData, 'June'),
                    ReportManager.monthlyInjectionReport(julyData, 'July'),
                    ReportManager.monthlyInjectionReport(augustData, 'August'),
                    ReportManager.monthlyInjectionReport(septemberData, 'September'),
                    ReportManager.monthlyInjectionReport(octoberData, 'October'),
                    ReportManager.monthlyInjectionReport(novemberData, 'November'),
                    ReportManager.monthlyInjectionReport(decemberData, 'December'),
                    ReportManager.titlePageWithList("Yearly Summary", ["Well Injection Summary", "Manifold Pressure Summary", "Daily Avg. per Month", "Field Notes"]),
                    wellInjectionPDF,
                    ReportManager.manifoldPressureSummary(manifoldPressures, pressureData, "Manifold Pressure Summary"),
                    ReportManager.dailyAvgSummary(avgInjData, oilProduced, ruralWater, "Daily Avg. per Month"),
                    ReportManager.monthlyNotes(notes, "Field Notes")
                ],
                styles: {
                    totals: {
                        bold: true
                    },
                    h1: {
                        fontSize: 24,
                        bold: true
                    },
                    h2: {
                        fontSize: 20,
                        bold: true
                    },
                    h3: {
                        fontSize: 16,
                        bold: true
                    }
                }
            }

            console.log("report data");
            console.dir(reportData);

            //pdfMake.createPdf(reportData).download($scope.selectedLease.name + "_" + $scope.selectedYear + ".pdf");
            //pdfMake.createPdf(reportData).download($scope.selectedLease.name + "_" + $scope.selectedYear + ".pdf");
            pdfMake.createPdf(reportData).open();

            setTimeout(function() {
                 $scope.$apply(function() {
                    $scope.progressBar = {
                        width: "0%"
                    }
                    $scope.disableControls = false;
                });
            }, 2000);

        });

    }

}]);