dz.controller("UserController", ['$scope', '$rootScope', '$localStorage', 'AuthService', 'users', function($scope, $rootScope, $localStorage, AuthService, users) {

    $rootScope.activeMenu = "Users";
    $rootScope.currentUser = $localStorage.user;

    $scope.users = users;
    $scope.message = "Changing password ...";

    $scope.changeInline = function(index) {
        $scope.users[index].editing = true;
    }

    $scope.cancelChange = function(index) {
        $scope.users[index].editing = false;
    }

    $scope.changePassword = function(index) {

        $scope.promise = AuthService.changeUserPassword($scope.users[index].id, $scope.users[index].newPassword)
        .then(function(data) {
            $scope.users[index].editing = false;
        });

    }

}]);