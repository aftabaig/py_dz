dz.controller("LeaseController", ['$scope', '$rootScope', '$localStorage', 'LeaseService', 'leases', function($scope, $rootScope, $localStorage, LeaseService, leases) {

  $rootScope.activeMenu = "Lease Management";
  $rootScope.currentUser = $localStorage.user;

  $scope.promise = null;
  $scope.message = "Updating ...";

  $scope.alert = {
    "show": false,
    "title": "",
    "message": ""
  }

  $scope.selectedIndex = -1;

  $scope.leases = leases;
  $scope.getLeases = function() {
    LeaseService.all().then(function(data) {
        $scope.leases = data;
    });
  };

  $scope.add = function() {
    var newLease = {
        "name": "New Lease",
        "isNew": true
    }
    $scope.leases.push(newLease);
  };

  $scope.cancelAdd = function(index) {
    $scope.leases.splice(index, 1);
  }

  $scope.save = function(index) {

    $scope.message = "Adding ...";

    $scope.promise = LeaseService.add($scope.leases[index]).then(function(data) {
        $scope.leases[index].isNew = false;
        $scope.leases[index].editing = false;
    });
  }

  $scope.editInline = function(index) {
    $scope.leases[index].editing = true;
  };

  $scope.cancelEditing = function(index) {
    $scope.leases[index].editing = false;
  };

  $scope.update = function(index) {

    $scope.message = "Updating";

    $scope.promise = LeaseService.update($scope.leases[index]).then(function(data) {
        $scope.leases[index].editing = false;
    });
  };

  $scope.confirmDelete = function(index) {
    $scope.selectedIndex = index;
    $scope.alert.title = "Delete " + $scope.leases[index].name;
    $scope.alert.message = "Are you sure to delete this lease?";
    $scope.alert.show = true;

  }

  $scope.delete = function() {

    $scope.message = "Deleting ...";

    $scope.promise = LeaseService.delete($scope.leases[$scope.selectedIndex].id).then(function(data) {
        $scope.leases.splice($scope.selectedIndex, 1);
        $scope.alert.show = false;
    });
  }

  $scope.hideAlert = function() {
    $scope.alert.show = false;
  }

}]);

