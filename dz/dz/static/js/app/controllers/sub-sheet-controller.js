dz.controller("SubSheetController", ['$scope', '$rootScope', '$localStorage', 'LeaseService', 'SheetService', 'SubSheetService', 'subSheets', 'sheet', 'lease', function($scope, $rootScope, $localStorage, LeaseService, SheetService, SubSheetService, subSheets, sheet, lease) {

  $rootScope.activeMenu = "Lease Management";
  $rootScope.currentUser = $localStorage.user;

  $scope.alert = {
    "show": false,
    "title": "",
    "message": ""
  }

  $scope.selectedIndex = -1;

  $scope.subSheets = subSheets;
  $scope.sheet = sheet;
  $scope.lease = lease;

  $scope.add = function() {
    var newSubSheet = {
        "lease": $scope.lease.id,
        "sheet": $scope.sheet.id,
        "name": "New Sub-sheet",
        "isNew": true
    }
    if (!$scope.subSheets) {
        $scope.subSheets = [];
    }
    $scope.subSheets.push(newSubSheet);
  };

  $scope.cancelAdd = function(index) {
    $scope.subSheets.splice(index, 1);
  }

  $scope.save = function(index) {
    SubSheetService.add($scope.subSheets[index]).then(function(data) {
        $scope.subSheets[index].isNew = false;
        $scope.subSheets[index].editing = false;
    });
  }

  $scope.editInline = function(index) {
    $scope.subSheets[index].editing = true;
  };

  $scope.cancelEditing = function(index) {
    $scope.subSheets[index].editing = false;
  };

  $scope.update = function(index) {
    SubSheetService.update($scope.subSheets[index]).then(function(data) {
        $scope.subSheets[index].editing = false;
    });
  };

  $scope.confirmDelete = function(index) {
    $scope.selectedIndex = index;
    $scope.alert.title = "Delete " + $scope.subSheets[index].name;
    $scope.alert.message = "Are you sure to delete this sub-sheet?";
    $scope.alert.show = true;

  }

  $scope.delete = function() {
    SubSheetService.delete($scope.subSheets[$scope.selectedIndex].id).then(function(data) {
        $scope.subSheets.splice($scope.selectedIndex, 1);
        $scope.alert.show = false;
    });
  }

  $scope.hideAlert = function() {
    $scope.alert.show = false;
  }

}]);