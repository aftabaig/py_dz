dz.controller("DataController", ['$scope', '$rootScope', '$localStorage', 'LeaseService', 'SheetService', 'SubSheetService', 'DataService', 'ReportService', 'lease', 'sheet', 'data', function($scope, $rootScope, $localStorage, LeaseService, SheetService, SubSheetService, DataService, ReportService, lease, sheet, data) {

    $rootScope.activeMenu = "Lease Management";
    $rootScope.currentUser = $localStorage.user;

    $scope.lease = lease;
    $scope.sheet = sheet;
    $scope.data = data;
    $scope.selectedDate = new Date();
    $scope.selectedMonth = new Date().getMonth() + 1;
    $scope.selectedYear = new Date().getFullYear();
    $scope.promise = null;
    $scope.message = "Please wait ...";

    $scope.totals = [];

    $scope.$watch("selectedDate", function(newDate, oldDate) {
        if (newDate != oldDate) {
            $scope.selectedMonth = newDate.getMonth() + 1;
            $scope.selectedYear = newDate.getFullYear();
            $scope.submit();
        }
    });

    $scope.editInline = function(dataItem) {
        dataItem.editing = true;
    }

    $scope.cancelEditing = function(dataItem) {
        dataItem.editing = false;
    };

    $scope.save = function(dataItem) {

        fieldValues = []
        dataItem.fields.forEach(function(field) {
            if (field.value != "") {
                fieldValue = {};
                if (field.value_id) {
                    fieldValue.id = field.value_id;
                }
                d = new Date(dataItem.date.month+"/"+dataItem.date.day+"/"+dataItem.date.year);
                fieldValue.date = d.getFullYear() + "-" + parseInt(d.getMonth()+1) + "-" + d.getDate();
                d = new Date()
                fieldValue.time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                fieldValue.sheet = sheet.id;
                fieldValue.field = field.field_id;
                fieldValue.value = field.value;
                fieldValues.push(fieldValue);
            }
        });

        $scope.message = "Updating ...";

        $scope.promise = DataService.post_data(fieldValues).then(function(data) {
                setTimeout(function() {
                    $scope.$apply(function() {
                        $rootScope.$broadcast("loading-complete");
                        dataItem.editing = false;
                    });
                }, 100);
        });

    }

    $scope.submit = function() {

        $scope.promise = DataService.data($scope.lease.id, $scope.sheet.id, $scope.selectedMonth, $scope.selectedYear).then(function(data) {

            $scope.data = data;
            $scope.data.forEach(function(dataItem) {
                var index=0;
                dataItem.fields.forEach(function(field) {
                    if (isNaN($scope.totals[index])) {
                        $scope.totals[index] = 0;
                    }
                    if (!isNaN(parseInt(field.value))) {
                        $scope.totals[index] += parseInt(field.value);
                    }
                    index++;
                });
            });

        });
    }

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    }

}]);