dz.factory("ReportService", function($http, $q, $localStorage) {
    var api_url = "/api/reports/";
    return {
        monthlyReport: function(leaseId, month, year) {
            var url = api_url + "lease/" + leaseId + "/month/" + month + "/year/" + year + "/";
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        injectionReport: function(leaseId, year) {
            var url = api_url + "lease/" + leaseId + "/year/" + year + "/type/" + "WELL/";
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                console.log("well");
                console.dir(data);
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        ruralWaterReport: function(leaseId, year) {
            var url = api_url + "lease/" + leaseId + "/year/" + year + "/type/" + "RURAL_WATER/";
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        oilProductionReport: function(leaseId, year) {
            var url = api_url + "lease/" + leaseId + "/year/" + year + "/type/" + "OIL_PRODUCED/";
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        manifoldPressureReport: function(leaseId, year) {
            var url = api_url + "lease/" + leaseId + "/year/" + year + "/type/" + "PRESSURE/";
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        notesData: function(leaseId, year) {
            var url = "/api/notes/lease/" + leaseId + "/year/" + year + "/";
             var defer = $q.defer();
             $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: url
             }).success(function(data, status, header, config) {
                defer.resolve(data);
             }).error(function(data, status, header, config) {
                defer.reject(status);
             });
             return defer.promise;
        }
    }
});