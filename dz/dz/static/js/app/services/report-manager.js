dz.factory("ReportManager", function() {

    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    return {

        // Monthly injection report.
        monthlyInjectionReport: function(data, month) {

            var table=[]
            var row=[]

            var col_totals=[]
            var col_counts=[]

            var i =0;

            data.forEach(function(data_row) {

                if (i == 0) {
                    row.push("Day");
                    data_row.fields.forEach(function(field) {
                        row.push(field.name);
                    });
                    table.push(row);
                }
                i++;

                row = [];
                row.push(data_row.date.day+"");
                var j=0;

                data_row.fields.forEach(function(field) {

                    if (!col_totals[j]) {
                        col_totals[j] = 0;
                    }

                    if (!col_counts[j]) {
                        col_counts[j] = 0;
                    }

                    if (!field.value || field.value === '') {
                        row.push({
                            text: "0.00",
                            alignment: 'right'
                        })
                    }
                    else {
                        var value = parseFloat(field.value);
                        col_totals[j] += value;
                        col_counts[j] += 1;
                        row.push({
                            text: value.toFixed(2),
                            alignment: 'right'
                        });
                    }
                    j++;

                });

                table.push(row);

            });

            row = [];
            row.push("TOT");
            col_totals.forEach(function(col_total) {
                row.push({
                    text: col_total.toFixed(2)+"",
                    bold: true,
                    alignment: 'right'
                });
            });
            table.push(row);

            row = [];
            row.push("AVG");
            for (i=0; i<col_totals.length; i++) {
                if (col_counts[i] > 0) {
                    row.push({
                        text: (col_totals[i]/col_counts[i]).toFixed(2)+"",
                        bold: true,
                        alignment: 'right'
                    });
                }
                else {
                    row.push({
                        text: "0.00",
                        bold: true,
                        alignment: 'right'
                    });
                }
            }
            table.push(row);

            var pdf =  [
                {
                    text: month + '\n\n',
                    bold: true
                },
                {
                    table: {
                        headerRows: 1,
                        body: table,
                    },
                    pageBreak: 'after'
                }
            ]
            return pdf;

        },

        // Yearly injection report for each well.
        wellInjectionSummary: function(names, data, title) {

            var table=[];
            var row=[];

            var grand_total=0;
            var grand_avg=0;

            var total_count=0;
            var avg_count=0;

            var row_totals=[];
            var row_avgs=[];
            var col_totals=[];
            var col_counts=[];

            row.push("");
            names.forEach(function(name) {
                row.push(name.report_title);
            });
            row.push("Avg./day");
            row.push("Avg./well/day");
            table.push(row);

            var i=0;
            var j=0;
            var count=0;
            data.forEach(function(data_row) {

                console.log("i: ",i);
                console.dir(data_row);

                count = 0;
                row = [];
                row.push(months[i]);

                j=0;
                data_row.forEach(function(cell) {

                    if (!row_totals[i]) {
                        row_totals[i] = 0;
                    }
                    if (!col_totals[j]) {
                        col_totals[j] = 0;
                        col_counts[j] = 0;
                    }

                    var cell_value = parseFloat(cell);
                    if (cell_value) {
                        row_totals[i] += cell_value;
                        col_totals[j] += cell_value;
                    }
                    if (cell_value > 0) {
                        count++;
                        col_counts[j] += 1;
                    }
                    row.push({
                        text: cell_value.toFixed(2),
                        alignment: 'right'
                    })
                    j++;
                });

                grand_total += row_totals[i];
                if (row_totals[i] > 0) {
                    total_count += 1;
                }

                row.push({
                    text: row_totals[i].toFixed(2),
                    alignment: 'right',
                    bold: true
                });

                if (count > 0) {
                    grand_avg += (row_totals[i]/count);
                    avg_count += 1;
                    row_avgs[i] = row_totals[i]/count;
                    row.push({
                        text: (row_avgs[i]).toFixed(2),
                        alignment: 'right',
                        bold: true
                    });
                }
                else {
                    row_avgs[i] = 0;
                    row.push({
                        text: "0.00",
                        alignment: 'right',
                        bold: true
                    });
                }
                table.push(row);
                i++;

            });

            i=0;
            row = [];
            row.push("");
            col_totals.forEach(function(col_total) {
                var col_avg = col_total/col_counts[i];
                i++;
                row.push({
                    text: col_avg.toFixed(2),
                    alignment: 'right',
                    bold: true
                });
            });
            if (total_count > 0) {
                row.push({
                    text: (grand_total/total_count).toFixed(2),
                    alignment: 'right',
                    bold: true
                });
            }
            else {
                row.push({
                    text: "0.00",
                    alignment: 'right',
                    bold: true
                });
            }
            if (avg_count > 0) {
                row.push({
                    text: (grand_avg/avg_count).toFixed(2),
                    alignment: 'right',
                    bold: true
                });
            }
            else {
                row.push({
                    text: "0.00",
                    alignment: 'right',
                    bold: true
                });
            }
            table.push(row);

            var pdf =  [
                {
                    text: title + '\n\n',
                    bold: true
                },
                {
                    table: {
                        headerRows: 1,
                        body: table,
                    },
                    pageBreak: 'after'
                }
            ]

            return {
                "pdf": pdf,
                "inj_avg": row_totals
            }

        },

        //Yearly Manifold pressure report.
        manifoldPressureSummary: function(names, data, title) {

            var table=[];
            var row=[];
            var col_totals=[];
            var col_counts=[];

            // Add all manifold-pressure fields.
            // Depending on the lease, there might be one field
            // or multiple fields.
            row.push("");
            names.forEach(function(name) {
                row.push({
                    text: name.report_title,
                    bold: true
                })
            });
            table.push(row);

            var i=0;
            var j=0;
            var count=0;
            data.forEach(function(data_row) {

                count = 0;
                row = [];
                row.push({
                    text: months[i],
                    bold: true
                })

                j=0;
                data_row.forEach(function(cell) {

                    if (!col_totals[j]) {
                        col_totals[j] = 0;
                    }

                    if (!col_counts[j]) {
                        col_counts[j] = 0;
                    }

                    var cell_value = parseFloat(cell);
                    if (cell_value) {
                        col_totals[j] += cell_value;
                    }
                    if (cell_value > 0) {
                        col_counts[j] += 1;
                    }
                    row.push({
                        text: cell_value.toFixed(2),
                        alignment: 'right'
                    });
                    j++;
                });
                table.push(row);
                i++;

            });

            row = [];

            row.push({
                text: "Year Avg.",
                bold: true
            });

            for (i=0; i<col_totals.length; i++) {
                if (col_counts[i] > 0) {
                    row.push({
                        text: (col_totals[i]/col_counts[i]).toFixed(2),
                        alignment: 'right',
                        bold: true
                    });
                }
                else {
                    row.push({
                        text: "0.00",
                        alignment: 'right',
                        bold: true
                    });
                }
            }
            table.push(row);

            var pdf =  [
                {
                    text: title + '\n\n',
                    bold: true
                },
                {
                    table: {
                        headerRows: 1,
                        body: table,
                    },
                    pageBreak: 'after'
                }
            ]
            return pdf;

        },

        // Daily Average Summary
        // @injAvg is a simple array of values each for the 12-months.
        // @production/@rural are arrays of objects returned by the API.
        dailyAvgSummary: function(injAvg, production, rural, title) {

            var table=[];
            var row=[];
            var col_totals=[];
            var col_counts=[];

            var productionForMonth;
            var ruralForMonth;
            var productionForMonth_value;
            var ruralForMonth_value;

            // Add titles.
            row.push("");
            row.push({
                text: "Inj Avg.",
                bold: true
            });
            row.push({
                text: "Production",
                bold: true
            });
            row.push({
                text: "Rural Water",
                bold: true
            });
            table.push(row);

            var i=0;
            injAvg.forEach(function(injAverageForMonth_value) {

                row = [];
                row.push({
                    text: months[i],
                    bold: true
                });

                productionForMonth = production[i];
                ruralForMonth = rural[i];

                productionForMonth_value = parseFloat(productionForMonth[0]);
                ruralForMonth_value = parseFloat(ruralForMonth[0]);

                row.push({
                    text: injAverageForMonth_value.toFixed(2),
                    alignment: 'right'
                });
                row.push({
                    text: productionForMonth_value.toFixed(2),
                    alignment: 'right'
                });
                row.push({
                    text: ruralForMonth_value.toFixed(2),
                    alignment: 'right'
                });

                table.push(row);

                i++;

            });

            var pdf =  [
                {
                    text: title + '\n\n',
                    bold: true
                },
                {
                    table: {
                        headerRows: 1,
                        body: table,
                    },
                    pageBreak: 'after'
                }
            ]
            return pdf;

        },

        monthlyNotes: function(notes, title) {

            var table=[];
            var row=[];

            notes.forEach(function(note) {

                row=[]

                // Append month name.
                row.push({
                    text: months[note.month-1],
                    bold: true
                });

                // Append notes in bulleted list.
                var arr_notes = note.notes.split('\n');
                row.push({
                    ul: arr_notes
                });

                table.push(row);

            });

            var pdf =  [
                {
                    text: title + '\n\n',
                    bold: true
                },
                {
                    table: {
                        headerRows: 0,
                        body: table,
                    },
                    pageBreak: 'after'
                }
            ]
            return pdf;
        },

        titlePage: function(title, subtitle) {

            return [
                "\n\n\n\n\n",
                {
                    text: title,
                    style: [
                        "h1"
                    ]
                },
                {
                    text: subtitle,
                    style: [
                        "h2"
                    ],
                    pageBreak: 'after'
                }
            ]

        },

        titlePageWithList: function(title, list) {

            var formattedList = [];
            list.forEach(function(listItem) {
                formattedList.push({
                    text: listItem,
                    bold: true
                });
            });

            return [
                "\n\n\n\n\n",
                {
                    text: title,
                    style: [
                        "h1"
                    ]
                },
                {
                    ol: formattedList,
                    pageBreak: 'after'
                }
            ]
        }
    }
});