dz.factory("AuthService", function($http, $q, $localStorage) {
    var api_url = "/api/users/";
    return {
        authenticate: function(username, password) {
            var url = api_url + "authenticate/";
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: url,
                data: {
                    "username": username,
                    "password": password
                }
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(data, status);
            });
            return defer.promise;
        },
        appUsers: function() {
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: api_url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(data, status);
            });
            return defer.promise;
        },
        changeUserPassword: function(userId, newPassword) {
            var url = api_url + "change_pwd/";
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'POST',
                url: url,
                data: {
                    "id": userId,
                    "password": newPassword
                }
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(data, status);
            });
            return defer.promise;
        }
    }
});