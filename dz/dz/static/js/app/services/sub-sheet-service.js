dz.factory("SubSheetService", function($http, $q) {
    var api_url = "/api/subSheets/";
    return {
        info: function(subSheetId) {
            var url = api_url + subSheetId + "/info";
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        add: function(subSheet) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: api_url,
                data: subSheet
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        update: function(subSheet) {
            var url = api_url + subSheet.id + "/";
            var defer = $q.defer();
            $http({
                method: 'PUT',
                url: url,
                data: subSheet
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
            console.dir(data);
                defer.reject(status);
            });
            return defer.promise;
        },
        delete: function(sub_sheet_id) {
            var url = api_url + sub_sheet_id + "/";
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        fields: function(sub_sheet_id) {
            var url = api_url + sub_sheet_id + "/fields/";
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
    }
});