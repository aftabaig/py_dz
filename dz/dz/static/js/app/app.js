function onError(e) {
  console.log(e);
}

// Create module.
var dz = angular.module('dz', ['ngResource', 'ngRoute', 'ngStorage', 'datePicker', 'ui.bootstrap', 'cgBusy']);

dz.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('^^');
  $interpolateProvider.endSymbol('^^');
});

dz.config(function($routeProvider) {
    $routeProvider
        .when("/login", {
            templateUrl: "static/js/app/views/login.html",
            controller: "LoginController",
            resolve: {

            }
        })
        .when("/leases", {
            templateUrl: "static/js/app/views/leases.html",
            controller: "LeaseController",
            resolve: {
                leases: function (LeaseService) {
                    return LeaseService.all();
                }
            }
        })
        .when("/leases/:id/sheets", {
            templateUrl: "static/js/app/views/sheets.html",
            controller: "SheetController",
            resolve: {
                sheets: function($route, LeaseService) {
                    var leaseId = $route.current.params.id;
                    return LeaseService.sheets(leaseId);
                },
                lease: function($route, LeaseService) {
                    var leaseId= $route.current.params.id;
                    return LeaseService.info(leaseId);
                }
            }
        })
        .when("/leases/:leaseId/sheets/:sheetId/subSheets", {
            templateUrl: "static/js/app/views/sub-sheets.html",
            controller: "SubSheetController",
            resolve: {
                subSheets: function($route, SheetService) {
                    var sheetId = $route.current.params.sheetId;
                    return SheetService.subSheets(sheetId);
                },
                sheet: function($route, SheetService) {
                    var sheetId = $route.current.params.sheetId;
                    return SheetService.info(sheetId);
                },
                lease: function($route, LeaseService) {
                    var leaseId = $route.current.params.leaseId;
                    return LeaseService.info(leaseId);
                }
            }
        })
        .when("/leases/:leaseId/sheets/:sheetId/fields", {
            templateUrl: "static/js/app/views/fields.html",
            controller: "FieldController",
            resolve: {
                fields: function($route, SheetService) {
                    var sheetId = $route.current.params.sheetId;
                    return SheetService.fields(sheetId);
                },
                subSheet: function($route) {
                    return null;
                },
                sheet: function($route, SheetService) {
                    var sheetId = $route.current.params.sheetId;
                    return SheetService.info(sheetId);
                },
                lease: function($route, LeaseService) {
                    var leaseId = $route.current.params.leaseId;
                    return LeaseService.info(leaseId);
                }
            }
        })
        .when("/leases/:leaseId/sheets/:sheetId/subSheets/:subSheetId/fields", {
            templateUrl: "static/js/app/views/fields.html",
            controller: "FieldController",
            resolve: {
                fields: function($route, SubSheetService) {
                    var subSheetId = $route.current.params.subSheetId;
                    return SubSheetService.fields(subSheetId);
                },
                subSheet: function($route, SubSheetService) {
                    var subSheetId = $route.current.params.subSheetId;
                    return SubSheetService.info(subSheetId);
                },
                sheet: function($route, SheetService) {
                    var sheetId = $route.current.params.sheetId;
                    return SheetService.info(sheetId);
                },
                lease: function($route, LeaseService) {
                    var leaseId = $route.current.params.leaseId;
                    return LeaseService.info(leaseId);
                }
            }
        })
        .when("/data/leases/:leaseId/sheets/:sheetId/month/:month/year/:year", {
            templateUrl: "static/js/app/views/data.html",
            controller: "DataController",
            resolve: {
                lease: function($route, LeaseService) {
                    var leaseId = $route.current.params.leaseId;
                    return LeaseService.info(leaseId);
                },
                sheet: function($route, SheetService) {
                    var sheetId = $route.current.params.sheetId;
                    return SheetService.info(sheetId);
                },
                data: function($route, DataService) {
                    var leaseId = $route.current.params.leaseId;
                    var sheetId = $route.current.params.sheetId;
                    var month = $route.current.params.month;
                    var year = $route.current.params.year;
                    return DataService.data(leaseId, sheetId, month, year);
                }
            }
        })
        .when("/reports", {
            templateUrl: "static/js/app/views/reports.html",
            controller: "ReportController",
            resolve: {
                leases: function($route, LeaseService) {
                    return LeaseService.all();
                }
            }
        })
        .when("/users", {
            templateUrl: "static/js/app/views/users.html",
            controller: "UserController",
            resolve: {
                users: function($route, AuthService) {
                    return AuthService.appUsers();
                }
            }
        })
        .otherwise({
            redirectTo: "/login"
        });
});

dz.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

dz.directive('routeLoader', function($rootScope) {
    return {
        link: function(scope, element) {
            element.addClass('ng-hide');
            var unRegister = $rootScope.$on('$routeChangeStart', function() {
                element.removeClass('ng-hide');
            });
            scope.$on('$destroy', unRegister);
        }
    };
});

dz.run(function($rootScope, $localStorage, $location) {
    $rootScope.$on("$routeChangeStart", function(event, next, current) {
        if (!$localStorage.token) {
            if (next.templateUrl === "static/js/app/views/login.html") {
            }
            else {
                $location.path("/login");
            }
        }
    });
    $rootScope.logout = function() {
        delete $localStorage.token;
        delete $localStorage.user;
        $rootScope.currentUser = null;
        $location.path("/login");
    }
});