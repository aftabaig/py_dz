dz.factory("SheetService", function($http, $q, $localStorage) {
    var api_url = "/api/sheets/";
    return {
        info: function(sheetId) {
            var url = api_url + sheetId + "/info";
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        add: function(sheet) {
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'POST',
                url: api_url,
                data: sheet
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        update: function(sheet) {
            var url = api_url + sheet.id + "/";
            var defer = $q.defer();
            console.dir(sheet);
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'PUT',
                url: url,
                data: sheet
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
            console.dir(data);
                defer.reject(status);
            });
            return defer.promise;
        },
        delete: function(sheet_id) {
            var url = api_url + sheet_id + "/";
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'DELETE',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        subSheets: function(sheet_id) {
            var url = api_url + sheet_id + "/subSheets/";
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        fields: function(sheet_id) {
            var url = api_url + sheet_id + "/fields/";
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
    }
});