dz.factory("DataService", function($http, $q, $localStorage) {
    var api_url = "/api/data/";
    return {
        data: function(leaseId, sheetId, month, year) {
            var url = api_url + "leases/" + leaseId + "/sheets/" + sheetId + "/month/" + month + "/year/" + year;
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'GET',
                url: url
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        },
        post_data: function(fieldValues) {
            var url = api_url;
            var defer = $q.defer();
            $http({
                headers: {
                    'Authorization': 'Token ' + $localStorage.token
                },
                method: 'POST',
                url: url,
                data: fieldValues
            }).success(function(data, status, header, config) {
                defer.resolve(data);
            }).error(function(data, status, header, config) {
                defer.reject(status);
            });
            return defer.promise;
        }
    }
});