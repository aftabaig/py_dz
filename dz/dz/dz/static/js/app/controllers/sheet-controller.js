dz.controller("SheetController", ['$scope', '$rootScope', '$localStorage', 'LeaseService', 'SheetService', 'sheets', 'lease', function($scope, $rootScope, $localStorage, LeaseService, SheetService, sheets, lease) {

  $rootScope.activeMenu = "Lease Management";
  $rootScope.currentUser = $localStorage.user;

  $scope.alert = {
    "show": false,
    "title": "",
    "message": ""
  }

  $scope.currentMonth = new Date().getMonth() + 1;
  $scope.currentYear = new Date().getFullYear();

  $scope.selectedIndex = -1;

  $scope.sheets = sheets;
  $scope.lease = lease;

  $scope.add = function() {
    var newSheet = {
        "lease": $scope.lease.id,
        "name": "",
        "has_sub_sheets": true,
        "isNew": true
    }
    if (!$scope.sheets) {
        $scope.sheets = [];
    }
    $scope.sheets.push(newSheet);
  };

  $scope.cancelAdd = function(index) {
    $scope.sheets.splice(index, 1);
  }

  $scope.save = function(index) {
    SheetService.add($scope.sheets[index]).then(function(data) {
        $scope.sheets[index].isNew = false;
        $scope.sheets[index].editing = false;
    });
  }

  $scope.editInline = function(index) {
    $scope.sheets[index].editing = true;
  };

  $scope.cancelEditing = function(index) {
    $scope.sheets[index].editing = false;
  };

  $scope.update = function(index) {
    SheetService.update($scope.sheets[index]).then(function(data) {
        $scope.sheets[index].editing = false;
    });
  };

  $scope.confirmDelete = function(index) {
    $scope.selectedIndex = index;
    $scope.alert.title = "Delete " + $scope.sheets[index].name;
    $scope.alert.message = "Are you sure to delete this sheet?";
    $scope.alert.show = true;

  }

  $scope.delete = function() {
    SheetService.delete($scope.sheets[$scope.selectedIndex].id).then(function(data) {
        $scope.sheets.splice($scope.selectedIndex, 1);
        $scope.alert.show = false;
    });
  }

  $scope.hideAlert = function() {
    $scope.alert.show = false;
  }

  $scope.toggleHasSubsheets = function(index) {
    $scope.sheets[index].has_sub_sheets = !$scope.sheets[index].has_sub_sheets;
  }

}]);