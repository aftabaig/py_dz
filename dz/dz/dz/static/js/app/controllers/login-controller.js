dz.controller("LoginController", ['$scope', '$rootScope', '$location', '$localStorage', 'AuthService', function($scope, $rootScope, $location, $localStorage, AuthService) {

    $scope.promise = null;
    $scope.message = "Authenticating ...";

    $scope.alert = {
        "show": false,
        "title": "asw",
        "message": ""
    }

    $scope.login = function() {

        $scope.promise = AuthService.authenticate($scope.username, $scope.password)
        .then(function(data) {

            $localStorage.token = data.token;
            $localStorage.user = {
                "username": $scope.username
            }
            $rootScope.currentUser = $localStorage.user;
            $location.path("/leases");

        }, function(error, status) {

            setTimeout(function() {
                $scope.$apply(function() {
                    $scope.alert.title = "Authentication Error";
                    $scope.alert.message = "Invalid username/password provided.";
                    $scope.alert.show = true;
                });
            }, 10);

            console.log($scope.alert.title);

        });
    };

}]);

