
from django.conf.urls import patterns, url, include
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('meta.views',
                       url(r'^api/users/$', 'app_users'),
                       url(r'^api/users/change_pwd', 'change_user_password'),
                       url(r'^api/leases/$', 'leases'),
                       url(r'^api/leases/([0-9]{1,2})/$', 'leases'),
                       url(r'^api/leases/([0-9]{1,2})/info/$', 'lease_info'),
                       url(r'^api/leases/([0-9]{1,2})/sheets/$', 'lease_sheets'),
                       url(r'^api/sheets/$', 'sheets'),
                       url(r'^api/sheets/([0-9]{1,2})/$', 'sheets'),
                       url(r'^api/sheets/([0-9]{1,2})/info/$', 'sheet_info'),
                       url(r'^api/sheets/([0-9]{1,2})/subSheets/$', 'sheet_subs'),
                       url(r'^api/sheets/([0-9]{1,2})/fields/$', 'sheet_fields'),
                       url(r'^api/subSheets/$', 'sub_sheets'),
                       url(r'^api/subSheets/([0-9]{1,2})/info/$', 'sub_sheet_info'),
                       url(r'^api/subSheets/([0-9]{1,2})/$', 'sub_sheets'),
                       url(r'^api/subSheets/([0-9]{1,2})/fields/$', 'sub_fields'),
                       url(r'^api/fields/$', 'fields'),
                       url(r'^api/fields/([0-9]{1,2})/$', 'fields'),
                       url(r'^api/data/leases/([0-9]{1,2})/sheets/([0-9]{1,2})/month/([0-9]{1,2})/year/([0-9]{1,4})/$', 'sheet_data'),
                       url(r'^api/data/$', 'post_data'),
                       url(r'^api/reports/lease/([0-9]{1,2})/year/([0-9]{1,4})/type/(\w{1,15})/$', 'report_data'),
                       url(r'^api/reports/lease/([0-9]{1,2})/month/([0-9]{1,4})/year/([0-9]{1,4})/$', 'monthly_report_data'),
                       url(r'^api/notes/lease/([0-9]{1,2})/year/([0-9]{1,4})/$', 'notes_data'),
)

urlpatterns += patterns('dz.views',
                        url(r'^$', 'home', name="home"))

urlpatterns += patterns('',
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
)

urlpatterns += patterns('',
                        url(r'^api/users/authenticate/', 'rest_framework.authtoken.views.obtain_auth_token'),
)


