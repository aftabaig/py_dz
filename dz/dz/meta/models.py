import datetime

from django.db import models


class Lease(models.Model):
    name = models.CharField('Lease Name', max_length=255, blank=False)
    createdAt = models.DateTimeField('Created At', auto_now=True, auto_now_add=True, default=datetime.datetime.now())
    updatedAt = models.DateTimeField('Updated At', auto_now=True, auto_now_add=True, default=datetime.datetime.now())


class Sheet(models.Model):
    name = models.CharField('Sheet Name', max_length=255, blank=False)
    lease = models.ForeignKey('Lease', default=0)
    has_sub_sheets = models.BooleanField('Has sub-sheets?')
    createdAt = models.DateTimeField('Created At', auto_now=True, auto_now_add=True, default=datetime.datetime.now())
    updatedAt = models.DateTimeField('Updated At', auto_now=True, auto_now_add=True, default=datetime.datetime.now())


class SubSheet(models.Model):
    name = models.CharField("Sub-sheet Name", max_length=255, blank=False)
    sheet = models.ForeignKey('Sheet', default=0)
    createdAt = models.DateTimeField('Created At', auto_now=True, auto_now_add=True, default=datetime.datetime.now())
    updatedAt = models.DateTimeField('Updated At', auto_now=True, auto_now_add=True, default=datetime.datetime.now())


class Field(models.Model):
    name = models.CharField('Field Name', max_length=255, blank=False)
    sheet = models.ForeignKey('Sheet', default=0)
    sub_sheet = models.ForeignKey('SubSheet', default=0, null=True, blank=True)
    data_type = models.CharField('Data Type', max_length=16, blank=False)
    main_value = models.FloatField('Minimum Value', default=0.0)
    max_value = models.FloatField('Maximum Value', default=0.0)
    report_type = models.CharField('Report Type', max_length=64)
    report_title = models.CharField('Report Title', max_length=256)
    calculate_total = models.BooleanField('Should total be calculated')
    calculate_average = models.BooleanField('Should average be calculated')
    createdAt = models.DateTimeField('Created At', auto_now=True, auto_now_add=True, default=datetime.datetime.now())
    updatedAt = models.DateTimeField('Updated At', auto_now=True, auto_now_add=True, default=datetime.datetime.now())


class FieldValue(models.Model):
    date = models.DateField('Date', default=datetime.datetime.now())
    time = models.TimeField('Time', default=datetime.datetime.now())
    sheet = models.ForeignKey('Sheet', default=0)
    sub_sheet = models.ForeignKey('SubSheet', default=0, null=True, blank=True)
    field = models.ForeignKey('Field', default=0)
    value = models.CharField('Field Value', max_length=1024)
    createdAt = models.DateTimeField('Created At', auto_now=True, auto_now_add=True, default=datetime.datetime.now())
    updatedAt = models.DateTimeField('Updated At', auto_now=True, auto_now_add=True, default=datetime.datetime.now())

