# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Lease'
        db.create_table(u'meta_lease', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('createdAt', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 10, 11, 0, 0), auto_now=True, auto_now_add=True, blank=True)),
            ('updatedAt', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 10, 11, 0, 0), auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'meta', ['Lease'])

        # Adding model 'Sheet'
        db.create_table(u'meta_sheet', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('lease', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['meta.Lease'])),
            ('has_sub_sheets', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('createdAt', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 10, 11, 0, 0), auto_now=True, auto_now_add=True, blank=True)),
            ('updatedAt', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 10, 11, 0, 0), auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'meta', ['Sheet'])

        # Adding model 'SubSheet'
        db.create_table(u'meta_subsheet', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('sheet', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['meta.Sheet'])),
            ('createdAt', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 10, 11, 0, 0), auto_now=True, auto_now_add=True, blank=True)),
            ('updatedAt', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 10, 11, 0, 0), auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'meta', ['SubSheet'])

        # Adding model 'Field'
        db.create_table(u'meta_field', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('sheet', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['meta.Sheet'])),
            ('sub_sheet', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['meta.SubSheet'], null=True, blank=True)),
            ('data_type', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('main_value', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('max_value', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('report_type', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('report_title', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('calculate_total', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('calculate_average', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('createdAt', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 10, 11, 0, 0), auto_now=True, auto_now_add=True, blank=True)),
            ('updatedAt', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 10, 11, 0, 0), auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'meta', ['Field'])

        # Adding model 'FieldValue'
        db.create_table(u'meta_fieldvalue', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 10, 11, 0, 0))),
            ('time', self.gf('django.db.models.fields.TimeField')(default=datetime.datetime(2014, 10, 11, 0, 0))),
            ('sheet', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['meta.Sheet'])),
            ('sub_sheet', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['meta.SubSheet'], null=True, blank=True)),
            ('field', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['meta.Field'])),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=1024)),
            ('createdAt', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 10, 11, 0, 0), auto_now=True, auto_now_add=True, blank=True)),
            ('updatedAt', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 10, 11, 0, 0), auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'meta', ['FieldValue'])


    def backwards(self, orm):
        # Deleting model 'Lease'
        db.delete_table(u'meta_lease')

        # Deleting model 'Sheet'
        db.delete_table(u'meta_sheet')

        # Deleting model 'SubSheet'
        db.delete_table(u'meta_subsheet')

        # Deleting model 'Field'
        db.delete_table(u'meta_field')

        # Deleting model 'FieldValue'
        db.delete_table(u'meta_fieldvalue')


    models = {
        u'meta.field': {
            'Meta': {'object_name': 'Field'},
            'calculate_average': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'calculate_total': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'createdAt': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'data_type': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_value': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'max_value': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'report_title': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'report_type': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'sheet': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': u"orm['meta.Sheet']"}),
            'sub_sheet': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': u"orm['meta.SubSheet']", 'null': 'True', 'blank': 'True'}),
            'updatedAt': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'})
        },
        u'meta.fieldvalue': {
            'Meta': {'object_name': 'FieldValue'},
            'createdAt': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)'}),
            'field': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': u"orm['meta.Field']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sheet': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': u"orm['meta.Sheet']"}),
            'sub_sheet': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': u"orm['meta.SubSheet']", 'null': 'True', 'blank': 'True'}),
            'time': ('django.db.models.fields.TimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)'}),
            'updatedAt': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '1024'})
        },
        u'meta.lease': {
            'Meta': {'object_name': 'Lease'},
            'createdAt': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updatedAt': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'})
        },
        u'meta.sheet': {
            'Meta': {'object_name': 'Sheet'},
            'createdAt': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'has_sub_sheets': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lease': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': u"orm['meta.Lease']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'updatedAt': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'})
        },
        u'meta.subsheet': {
            'Meta': {'object_name': 'SubSheet'},
            'createdAt': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'sheet': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': u"orm['meta.Sheet']"}),
            'updatedAt': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 10, 11, 0, 0)', 'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['meta']