from rest_framework import serializers

# models
from models import Lease
from models import Sheet
from models import SubSheet
from models import Field
from models import FieldValue
from django.contrib.auth.models import User

class LeaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lease


class SheetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sheet


class SubSheetSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubSheet


class FieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = Field


class FieldValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldValue


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
