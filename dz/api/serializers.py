from rest_framework import serializers

# models
from models import Entity
from models import EntityRelationship
from models import Consignment
from models import ConsignmentItem


class RelationshipSerializer(serializers.ModelSerializer):
    class Meta:
        model = EntityRelationship


class EntitySerializer(serializers.ModelSerializer):
    entity_relationships = RelationshipSerializer(many=True, allow_add_remove=True, required=False)

    class Meta:
        model = Entity
        fields = ('id',
                  'name',
                  'type',
                  'tenancy',
                  'street_num',
                  'street',
                  'town',
                  'postcode',
                  'state',
                  'country',
                  'entity_relationships')


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConsignmentItem


class ConsignmentSerializer(serializers.ModelSerializer):
    items = ItemSerializer(many=True, allow_add_remove=True)
    consignor = EntitySerializer(many=False)
    consignee = EntitySerializer(many=False)

    class Meta:
        model = Consignment
        fields = ('consignor',
                  'consignee',
                  'account',
                  'mode',
                  'status',
                  'pickupDate',
                  'items')

