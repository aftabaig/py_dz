
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.db import connection
from calendar import monthrange
import datetime

# serializers
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import IsAdminUser

from serializers import LeaseSerializer
from serializers import SheetSerializer
from serializers import SubSheetSerializer
from serializers import FieldSerializer
from serializers import FieldValueSerializer
from serializers import UserSerializer

# models
from models import Lease
from models import Sheet
from models import SubSheet
from models import Field
from models import FieldValue
from django.contrib.auth.models import User


def to_dictionary(cursor):
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


@api_view(['GET', 'POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated, ])
def leases(request, id=0):
    if request.method == 'GET':
        all_leases = Lease.objects.all().order_by('id')
        serializer = LeaseSerializer(all_leases, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = LeaseSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'PUT':
        lease = Lease(id=id)
        serializer = LeaseSerializer(lease, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        lease = Lease(id=id)
        lease.delete()
        return Response("DELETED", status=status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def lease_info(request, id=0):
    lease = Lease.objects.get(pk=id)
    serializer = LeaseSerializer(lease)
    return Response(serializer.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def lease_sheets(request, id=0):
    sheets = Sheet.objects.select_related('lease').filter(lease__id=id).order_by('id')
    serializer = SheetSerializer(sheets, many=True)
    return Response(serializer.data)


@api_view(['POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated, ])
def sheets(request, id=0):
    if request.method == 'POST':
        serializer = SheetSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'PUT':
        sheet = Sheet.objects.select_related('lease').get(pk=id)
        serializer = SheetSerializer(sheet, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        sheet = Sheet(id=id)
        sheet.delete()
        return Response("DELETED", status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def sheet_info(request, id=0):
    sheet = Sheet.objects.select_related('lease').get(pk=id)
    serializer = SheetSerializer(sheet)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def sheet_subs(request, id=0):
    sub_sheets = SubSheet.objects.select_related('sheet').filter(sheet__id=id).order_by('id')
    serializer = SubSheetSerializer(sub_sheets, many=True)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def sheet_fields(request, id=0):
    fields = Field.objects.select_related('sheet').filter(sheet__id=id).order_by('id')
    serializer = FieldSerializer(fields, many=True)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def sheet_field_values(request, id=0):
    if request.method == 'GET':
        values = FieldValue.objects.select_related('sheet').filter(sheet__id=id).order_by('date')
        serializer = FieldValueSerializer(values, many=True)
        return Response(serializer.data)


@api_view(['POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated, ])
def sub_sheets(request, id=0):
    if request.method == 'POST':
        serializer = SubSheetSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'PUT':
        sub_sheet = SubSheet(id=id)
        serializer = SubSheetSerializer(sub_sheet, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        sub_sheet = SubSheet(id=id)
        sub_sheet.delete()
        return Response("DELETED", status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def sub_sheet_info(request, id=0):
    sub_sheet = SubSheet.objects.select_related('sheet').get(pk=id)
    serializer = SubSheetSerializer(sub_sheet)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def sub_fields(request, id=0):
    fields = Field.objects.select_related('sub_sheet').filter(sub_sheet__id=id).order_by('id')
    serializer = FieldSerializer(fields, many=True)
    return Response(serializer.data)


@api_view(['POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated, ])
def fields(request, id=0):
    if request.method == 'POST':
        serializer = FieldSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'PUT':
        field = Field(id=id)
        serializer = FieldSerializer(field, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        field = Field(id=id)
        field.delete()
        return Response("DELETED", status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated, ])
def sheet_data(request, lease_id=0, sheet_id=0, month=1, year=2014):
    if request.method == 'GET':

        cursor = connection.cursor()
        cursor.execute('SELECT * FROM meta_field WHERE sheet_id = %s ORDER BY id', (sheet_id,))
        all = to_dictionary(cursor)

        cursor.execute("""
                        SELECT id, field_id, value, date_part('day', date) as day FROM meta_fieldvalue
                        WHERE sheet_id = %s
                        AND date_part('month', date) = %s
                        AND date_part('year', date) = %s;
                        """, (sheet_id, month, year,))
        all_values = to_dictionary(cursor)

        results = []
        days = monthrange(int(year), int(month))[1]
        for day in range(1, days+1):

            date = {
                "day": day,
                "month": month,
                "year": year
            }
            fields = []
            for row in all:

                field = {
                    "field_id": row['id'],
                    "name": row['name'],
                    "data_type": row['data_type'],
                    "min_value": row['main_value'],
                    "max_value": row['max_value'],
                    "value": '',
                }

                for v in all_values:
                    if v['field_id'] == field['field_id'] and v['day'] == day:
                        field['value_id'] = v['id']
                        field['value'] = v['value']

                fields.append(field)
            result = {
                "date": date,
                "fields": fields
            }
            results.append(result)

        return Response(results, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes([IsAuthenticated, ])
def post_data(request):

    sheet_id = request.DATA[0]['sheet']
    date = request.DATA[0]['date']

    field_values = FieldValue.objects.raw("""
                            SELECT * FROM meta_fieldvalue
                            WHERE sheet_id = %s
                            AND date = %s
                           """, (sheet_id, date))

    serializer = FieldValueSerializer(field_values, data=request.DATA, many=True, allow_add_remove=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=HTTP_201_CREATED)
    else:
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAdminUser, ])
def report_data(request, lease_id, year=2014, type='WELL'):

    cursor = connection.cursor()
    cursor.execute("SELECT id, name, report_title "
                   "FROM meta_field "
                   "WHERE report_type = %s"
                   "AND sheet_id IN "
                   "(SELECT id FROM meta_sheet WHERE lease_id = %s)", (type, lease_id, ))
    fields = to_dictionary(cursor)

    cursor.execute("SELECT CAST(AVG(CAST(mfv.value AS DECIMAL(9,2))) AS DECIMAL(9,2)) AS v, "
                   "mfv.field_id, mf.name, mfv.field_id, "
                   "date_part('month', mfv.date) AS month, date_part('year', mfv.date) AS year "
                   "FROM meta_field mf "
                   "LEFT OUTER JOIN meta_fieldvalue mfv "
                   "ON mf.id = mfv.field_id "
                   "WHERE mf.report_type = %s "
                   "AND mf.sheet_id IN (SELECT id FROM meta_sheet WHERE lease_id = %s) "
                   "AND date_part('year', mfv.date) = %s"
                   "GROUP BY mfv.field_id, mf.name , mfv.field_id, "
                   "date_part('month', mfv.date), date_part('year', mfv.date) "
                   "ORDER BY month, year, mfv.field_id", (type, lease_id, year,))
    report_fields = to_dictionary(cursor)

    arr_report_field = []
    for month in range(1, 13):
        arr_report_field_month = []
        for field in fields:
            field_found = False
            for report_field in report_fields:
                if report_field['month'] == month and report_field['field_id'] == field['id']:
                    field_found = True
                    arr_report_field_month.append(report_field['v'])
            if not field_found:
                arr_report_field_month.append("0")
        arr_report_field.append(arr_report_field_month)


    report = {
        "names": fields,
        "data": arr_report_field
    }

    return Response(report, status=status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes([IsAdminUser, ])
def monthly_report_data(request, lease_id=0, month=1, year=2014):
    if request.method == 'GET':

        cursor = connection.cursor()
        cursor.execute("SELECT id, report_title "
                       "FROM meta_field "
                       "WHERE report_type = 'WELL'"
                       "AND sheet_id IN "
                       "(SELECT id FROM meta_sheet WHERE lease_id = %s)", (lease_id, ))
        all = to_dictionary(cursor)

        cursor.execute("""
                        SELECT mfv.id, mfv.field_id, mfv.value, date_part('day', date) as day FROM meta_fieldvalue mfv
                        INNER JOIN meta_field mf ON mfv.field_id = mf.id
                        WHERE mfv.sheet_id IN (SELECT id FROM meta_sheet WHERE lease_id = %s)
                        AND mf.report_type = 'WELL'
                        AND date_part('month', date) = %s
                        AND date_part('year', date) = %s;
                        """, (lease_id, month, year,))
        all_values = to_dictionary(cursor)

        results = []
        days = monthrange(int(year), int(month))[1]
        for day in range(1, days+1):

            date = {
                "day": day,
                "month": month,
                "year": year
            }
            fields = []
            for row in all:

                field = {
                    "field_id": row['id'],
                    "name": row['report_title'],
                    "value": '',
                }

                for v in all_values:
                    if v['field_id'] == field['field_id'] and v['day'] == day:
                        field['value_id'] = v['id']
                        field['value'] = v['value']

                fields.append(field)
            result = {
                "date": date,
                "fields": fields
            }
            results.append(result)

        return Response(results, status=status.HTTP_201_CREATED)


@api_view(['GET'])
@permission_classes([IsAdminUser, ])
def notes_data(request, lease_id, year=2014):

    cursor = connection.cursor()
    cursor.execute("SELECT CAST(date_part('month', date) AS Integer) AS month, string_agg(value, '\n') AS notes "
                   "FROM meta_fieldvalue mfv "
                   "LEFT OUTER JOIN meta_field mf "
                   "ON mfv.field_id = mf.id "
                   "WHERE report_type = 'NOTES' "
                   "AND date_part('year', date) = %s"
                   "AND mf.sheet_id IN "
                   "(SELECT id FROM meta_sheet WHERE lease_id = %s) "
                   "GROUP BY "
                   "date_part('month', date), date_part('year', date) "
                   "ORDER BY month", (year, lease_id, ))
    notes = to_dictionary(cursor)

    monthly_notes = []
    for month in range(1, 13):
        month_found = False
        for note in notes:
            if month == note["month"]:
                month_found = True
                month_notes = {
                    "month": month,
                    "notes": note["notes"]
                }
                monthly_notes.append(month_notes)
        if not month_found:
            month_notes = {
                "month": month,
                "notes": ""
            }
            monthly_notes.append(month_notes)

    return Response(monthly_notes, status=status.HTTP_201_CREATED)


@api_view(['GET'])
@permission_classes([IsAdminUser, ])
def app_users(request):
    users = User.objects.filter(is_superuser=False)
    serializer = UserSerializer(users, read_only=True, many=True)
    return Response(serializer.data)

@api_view(['POST'])
@permission_classes([IsAdminUser, ])
def change_user_password(request):
        user = User.objects.get(pk=int(request.DATA.get("id")))
        user.set_password(request.DATA.get("password"))
        user.save()
        res = {
            "success": True
        }
        return Response(res, status=status.HTTP_200_OK)




